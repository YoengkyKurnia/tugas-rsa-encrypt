<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>RSA</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading border-bottom bg-light">RSA Generator</div>
                <div class="list-group list-group-flush">
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/">Dashboard</a>
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/generate">Generate Public & Key</a>
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/signingRsa">Signing Document</a>
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/verifySignRsa">Verify Signed Document</a>
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/encrypt">Encrypt File</a>
                    <a class="list-group-item list-group-item-action list-group-item-light p-3" href="/decrypt">Decrypt FIle</a>
                </div>
            </div>
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <script src="{{asset('/js/scripts.js')}}"></script>
        @yield('javascript')
    </body>
</html>
