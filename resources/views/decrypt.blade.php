@extends('partial.main')


@section('content')
    <style>
        .hide{
            display: none;
        }
    </style>
    <h1 class="mt-4">RSA Generator</h1>
    <br>
    <form action="/decryptChiper" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <label for="">Chiper Text :</label>
                <input type="file" name="chiperText" class="form-control" required>
            </div>
            <div class="col-lg-4">
                <label for="">Private Key :</label>
                <input type="file" name="privateKey" class="form-control" required>
                @if(Session::get('status') != null && Session::get('status') == true)
                    <br>
                    <textarea readonly cols="30" rows="10" class="form-control" required>{{Session::get('status')['data']}}</textarea>
                    <br>
                    <a href="{{asset('uploads/userPlainText.txt')}}" download="userPlainText.txt" class="downloadLink">Download Plain Text</a>
                @endif
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-2">
                <button class="btn btn-primary" type="submit" id="generator">Generate Now!</button>
            </div>
        </div>
    </form>
@endsection
