@extends('partial.main')


@section('content')
    <style>
        .hide{
            display: none;
        }
    </style>
    <h1 class="mt-4">RSA Generator</h1>
    <br>
    <form action="/encryptChiper" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <label for="">Plain Text :</label>
                <textarea name="plainText" id="plainText" cols="30" rows="10" class="form-control" required></textarea>
            </div>
            <div class="col-lg-4">
                <label for="">Public Key :</label>
                <input type="file" name="publicKey" class="form-control" required>
                @if(Session::get('status') != null && Session::get('status') == true)
                    <br>
                    <textarea readonly cols="30" rows="10" class="form-control" required>{{Session::get('status')['data']}}</textarea>
                    <br>
                    <a href="{{asset('uploads/chiperText.txt')}}" download="chiperText.txt" class="downloadLink">Download Chiper Text</a>
                @endif
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-2">
                <button class="btn btn-primary" type="submit" id="generator">Generate Now!</button>
            </div>
        </div>
    </form>
@endsection
