@extends('partial.main')


@section('content')
    <style>
        .hide{
            display: none;
        }
    </style>
    <h1 class="mt-4">RSA Generator</h1>
    <br>
    <div class="row">
        <div class="col-lg-2">
            <button class="btn btn-primary" type="button" id="generator">Generate Now!</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-4">
            <label for="">Public Key :</label>
            <textarea name="publicKey" id="publicKey" cols="30" rows="10" class="form-control" readonly></textarea>
            <a href="{{asset('uploads/publickey.pub')}}" download="publicKey.pub" class="downloadLink hide">Download Public Key</a>
        </div>
        <div class="col-lg-4">
            <label for="">Private Key :</label>
            <textarea name="privateKey" id="privateKey" cols="30" rows="10" class="form-control" readonly></textarea>
            <a href="{{asset('uploads/privatekey.pri')}}" download="privateKey.pri" class="downloadLink hide">Download Private Key</a>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).on("click","#generator",function(){
            $.ajax({
                url: "/generateKey",
                success: function(data){
                    $('#publicKey').empty();
                    $('#privateKey').empty();
                    $('#publicKey').text(data.publicKey);
                    $('#privateKey').text(data.privateKey);
                    $('.downloadLink').removeClass('hide');
                }
            });
        });
    </script>
@endsection
