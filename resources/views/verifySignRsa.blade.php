@extends('partial.main')


@section('content')
    <style>
        .hide{
            display: none;
        }
    </style>
    <h1 class="mt-4">RSA Generator</h1>
    <br>
    <form action="/verifySignRsaPost" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <label for="">Chiper With Sign :</label>
                <input type="file" name="chiperTextSign" class="form-control" required>
            </div>
            <div class="col-lg-4">
                <label for="">Private Key :</label>
                <input type="file" name="privateKey" class="form-control" required>
            </div>
            <div class="col-lg-4">
                <label for="">Public Key :</label>
                <input type="file" name="publicKey" class="form-control" required>
                @if(Session::get('status') != null)
                    <br>
                    <textarea readonly cols="30" rows="10" class="form-control" required>{{Session::get('status')['data']}}</textarea>
                    <br>
                    @if(Session::get('status')['status'] == true)
                        <a href="{{asset('uploads/userPlainText.txt')}}" download="userPlainText.txt" class="downloadLink">Download Plain Text</a>
                    @endif
                @endif
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-2">
                <button class="btn btn-primary" type="submit" id="generator">Generate Now!</button>
            </div>
        </div>
    </form>
@endsection
