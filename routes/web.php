<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('generate','GenerateKeyController@index');
Route::get('generateKey','GenerateKeyController@generateKey');
Route::get('encrypt','GenerateKeyController@encrypt');
Route::post('encryptChiper','GenerateKeyController@encryptChiper');
Route::get('decrypt','GenerateKeyController@decrypt');
Route::post('decryptChiper','GenerateKeyController@decryptChiper');
Route::get('signingRsa','GenerateKeyController@signingRsa');
Route::post('signingRsaPost','GenerateKeyController@signingRsaPost');
Route::get('verifySignRsa','GenerateKeyController@verifySignRsa');
Route::post('verifySignRsaPost','GenerateKeyController@verifySignRsaPost');



