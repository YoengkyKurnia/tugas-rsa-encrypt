<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GenerateKeyController extends Controller
{
    public function index()
    {
        return view('generate');
    }

    public function generateKey()
    {

        //Config RSA
        $config = array(
            'private_key_bits' => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        //Get Public and Private Key
        $res = openssl_pkey_new($config);
        openssl_pkey_export($res, $privKey);
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];


        //Store Public Key
        $output_file = public_path('uploads'). '/publickey.pub';
        $ifp = fopen( $output_file, 'wb' );
        fwrite( $ifp, $pubKey);
        fclose( $ifp );


        //Store Private Key
        $output_file = public_path('uploads'). '/privatekey.pri';
        $ifp = fopen( $output_file, 'wb' );
        fwrite( $ifp, $privKey);
        fclose( $ifp );
        ob_clean();


        $output = [
            'publicKey' => $pubKey,
            'privateKey' => $privKey
        ];
        return $output;
    }


    public function encrypt()
    {
        return view('encrypt');
    }

    public function encryptChiper(Request $request)
    {
        //Upload File User
        $file = $request->file('publicKey');
        $destinationPath = 'uploads';
        $extension = $file->getClientOriginalExtension();
        $fileName = 'userPublicKey.' . $extension;
        $file->move($destinationPath, $fileName);


        //Generate RSA Encrypt
        $source = $request->plainText;
        $output_file = public_path('uploads'). '/userPublicKey.pub';
        $fp = fopen($output_file,"r");
        $pub_key = fread($fp,2048);
        fclose($fp);
        openssl_get_publickey($pub_key);
        openssl_public_encrypt($source, $crypttext, $pub_key);
        $hasil = bin2hex($crypttext);


        //Generate File Chiper
        $output_file = public_path('uploads'). '/chiperText.txt';
        $ifp = fopen( $output_file, 'wb' );
        fwrite( $ifp, $hasil);
        fclose( $ifp );


        //redirect
        $output = [
            'status' => true,
            'data' => $hasil
        ];
        return redirect()->back()->with('status',$output);
    }

    public function decrypt()
    {
        return view('decrypt');
    }

    public function decryptChiper(Request $request)
    {
        //Upload File privateKey User
        $file = $request->file('privateKey');
        $destinationPath = 'uploads';
        $extension = $file->getClientOriginalExtension();
        $fileName = 'userPrivateKey.' . $extension;
        $file->move($destinationPath, $fileName);


        //Upload File Chiper Text User
        $file2 = $request->file('chiperText');
        $destinationPath2 = 'uploads';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = 'userChiperText.' . $extension2;
        $file2->move($destinationPath2, $fileName2);


        //GetChiper
        $output_file = public_path('uploads'). '/'.$fileName2;
        $fp=fopen($output_file,"r");
        $crypttext = fread($fp,2048);
        fclose($fp);

        //Decrypt RSA
        $output_file = public_path('uploads'). '/'.$fileName;
        $fp=fopen($output_file,"r");
        $privkey = fread($fp,2048);
        fclose($fp);
        $res = openssl_pkey_get_private($privkey);
        openssl_private_decrypt(hex2bin($crypttext), $newsource, $res);


        //Generate Plain Text
        $source = $request->plainText;
        $output_file = public_path('uploads'). '/userPlainText.txt';
        $fp = fopen($output_file,"wb");
        fwrite($fp, $newsource);
        fclose($fp);

        $output = [
            'status' => true,
            'data' => $newsource
        ];
        return redirect()->back()->with('status',$output);
    }

    public function signingRsa()
    {
        return view('signingRsa');
    }

    public function signingRsaPost(Request $request)
    {
        //Upload File privateKey User
        $file = $request->file('privateKey');
        $destinationPath = 'uploads';
        $extension = $file->getClientOriginalExtension();
        $fileName = 'userPrivateKey.' . $extension;
        $file->move($destinationPath, $fileName);

        //Upload File Chiper Text User
        $file2 = $request->file('chiperText');
        $destinationPath2 = 'uploads';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = 'userChiperText.' . $extension2;
        $file2->move($destinationPath2, $fileName2);

        //GetChiper
        $output_file = public_path('uploads'). '/'.$fileName2;
        $fp=fopen($output_file,"r");
        $chiperText = fread($fp,2048);
        fclose($fp);

        //Get Private
        $output_file = public_path('uploads'). '/'.$fileName;
        $fp=fopen($output_file,"r");
        $privKey = fread($fp,2048);
        fclose($fp);

        //Generate Sign
        openssl_sign($chiperText,$signature,$privKey,OPENSSL_ALGO_SHA1);
        $sign = '<ds>'.bin2hex($signature).'</ds>';
        $combine = $chiperText . $sign;


        //Generate File Chiper With Sign
        $output_file = public_path('uploads'). '/chiperTextWithSign.txt';
        $ifp = fopen( $output_file, 'wb' );
        fwrite( $ifp, $combine);
        fclose( $ifp );

        $output = [
            'status' => true,
            'data' => $combine
        ];
        return redirect()->back()->with('status',$output);
    }

    public function verifySignRsa()
    {
        return view('verifySignRsa');
    }

    function verifySignRsaPost(Request $request)
    {
        //Upload File chiperSigned User
        $file = $request->file('chiperTextSign');
        $destinationPath = 'uploads';
        $extension = $file->getClientOriginalExtension();
        $fileName = 'chiperTextSign.' . $extension;
        $file->move($destinationPath, $fileName);


        //Upload Public Key
        $file2 = $request->file('publicKey');
        $destinationPath2 = 'uploads';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = 'userPublicKey.' . $extension2;
        $file2->move($destinationPath2, $fileName2);

        //Upload Public Key
        $file2 = $request->file('privateKey');
        $destinationPath2 = 'uploads';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName3 = 'userPrivateKey.' . $extension2;
        $file2->move($destinationPath2, $fileName3);


        //GetChiperSigned
        $output_file = public_path('uploads'). '/'.$fileName;
        $fp=fopen($output_file,"r");
        $crypttextsign = fread($fp,2048);
        fclose($fp);

        //Public Key
        $output_file = public_path('uploads'). '/'.$fileName2;
        $fp=fopen($output_file,"r");
        $pubKey = fread($fp,2048);
        fclose($fp);

        //Private Key
        $output_file = public_path('uploads'). '/'.$fileName3;
        $fp=fopen($output_file,"r");
        $privKey = fread($fp,2048);
        fclose($fp);

        //Exploded
        $explode = explode('<ds>',$crypttextsign);
        $chiperExploded = $explode[0];
        $signature = hex2bin(str_replace('</ds>','',$explode[1]));


        //verify signature
        $ok = openssl_verify($chiperExploded, $signature, $pubKey, OPENSSL_ALGO_SHA1);

        if ($ok == 1){
                $res = openssl_pkey_get_private($privKey);
                openssl_private_decrypt(hex2bin($chiperExploded), $newsource, $res);

                //Generate Plain Text
                $source = $request->plainText;
                $output_file = public_path('uploads'). '/userPlainText.txt';
                $fp = fopen($output_file,"wb");
                fwrite($fp, $newsource);
                fclose($fp);

                $output = [
                    'status' => true,
                    'data' => $newsource
                ];

        } elseif ($ok == 0){
                $output = [
                    'status' => false,
                    'data' => 'Bad Request'
                ];
        } else {
            $output = [
                'status' => false,
                'data' => 'Bad Request, error checking signature'
            ];
        }
        return redirect()->back()->with('status',$output);
    }
}
